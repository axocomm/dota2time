import json

def main():
    text = open("live.json").read()
    j = json.loads(text)
    # print j["result"]["games"][0]["players"]
    players = team_split(j["result"]["games"][0]["players"])
    print players

def team_split(players):
    radiantList = []
    direList = []
    for player in players:
        if player["team"] is 0:
            radiantList.append(player)
        elif player["team"] is 1:
            direList.append(player)

    return {"radiant": radiantList, "dire": direList}

if __name__ == "__main__":
    main()
