import json

def main():
    hero_txt = open("heroes.json").read()
    hero_json = json.loads(hero_txt)
    heroes = hero_assign(hero_json["heroes"])

def hero_assign(heroes):
    hero_dict = {}
    for hero in heroes:
        hero_dict[hero["id"]] = hero["localized_name"]
    print hero_dict
    return hero_dict

if __name__ == "__main__":
    main()
