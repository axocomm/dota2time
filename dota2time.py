import json
from dota_api import Dota
from flask import Flask, request, session, g, redirect, url_for, abort, \
    render_template, flash
from requests import get

from leaguematch import league_assign
from teamsplit import team_split
from getitems import item_assign
from getheroes import hero_assign

import re
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

APIKEY = '8C196C8AAA4A4D2BAAA5503A190FBA72'
URL_FORMAT = 'http://api.steampowered.com/IDOTA2Match_570/%s/v1/?key=%s'
DEBUG = True

app = Flask(__name__)
app.config.from_object(__name__)

def convert(input):
    """Convert the utf8 to normal strings."""
    if isinstance(input, dict):
        return {convert(key): convert(value) for key, value in input.iteritems()}
    elif isinstance(input, list):
        return [convert(element) for element in input]
    elif isinstance(input, unicode):
        return input.encode('utf-8')
    else:
        return input

def get_display_match(match):
    if 'radiant_team' in match.keys() and 'dire_team' in match.keys():
        radiant_team = match['radiant_team']['team_name']
        dire_team = match['dire_team']['team_name']
    else:
        radiant_team, dire_team = 'Radiant', 'Dire'

    display_match = {
        'match_id': match['match_id'],
        'teams': {
            'radiant': radiant_team,
            'dire': dire_team
        }
    }

    return display_match

def get_home_panel(match, leagues, size=4):
    league = leagues[match['league_id']] if match['league_id'] in leagues else 'NA'
    display_match = get_display_match(match)
    display_match['league'] = league
    display_match['panel_size'] = size

    return display_match

def get_home_panels(matches):
    leagues = league_assign(g.api.get_league_listing()['result'])
    match_panels = map(lambda g: get_home_panel(g, leagues),
                       matches['result']['games'])

    return [
        match_panels[:3]
    ]

@app.before_request
def init():
    g.api = Dota(app.config['APIKEY'])

@app.route('/')
def index():
    return render_template('layout.html')

@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/dashboard')
def dashboard():
    panels = get_home_panels(g.api.get_live_league_games())
    return render_template('dashboard.html', rows=panels)

def get_match_by_id(matches, match_id):
    for match in matches:
        if int(match['match_id']) == int(match_id):
            return match
    return None

def convert_scoreboard(scoreboard):
    compressed = scoreboard['radiant']['players'] + scoreboard['dire']['players']
    keys = map(lambda p: p['account_id'], compressed)
    return dict(zip(keys, compressed))

def map_items(score, items):
    for k in filter(lambda a: re.search(r'^item', a), score.keys()):
        score[k] = items[score[k]] if score[k] in items else 'Unknown'
    return score

def map_heroes(score, heroes):
    print heroes.keys()
    score['hero'] = heroes[int(score['hero_id'])]

    return score

def do_maps(score, items, heroes):
    return map_heroes(map_items(score, items), heroes)

@app.route('/match/<match_id>')
def match_details(match_id):
    # match = g.api.get_match_details(match_id)['result']
    matches = convert(json.loads(open('live.json').read())['result']['games'])
    match = get_match_by_id(matches, match_id)
    if match is None:
        return render_template('details.html', error='Match not found')
    display_match = get_display_match(match)
    scores = convert_scoreboard(match['scoreboard'])

    account_ids = scores.keys()
    items = item_assign(json.loads(open('items.json').read())['items'])
    heroes = hero_assign(json.loads(open('heroes.json').read())['heroes'])
    scores = dict(zip(account_ids, map(lambda s: do_maps(scores[s], items, heroes),
                                       scores)))
    display_teams = team_split(match['players'])

    return render_template('details.html', match=display_match,
                           teams=display_teams, scores=scores)

@app.route('/foo')
def foo():
    return render_template('panels/foo.html')

if __name__ == '__main__':
    app.run()
