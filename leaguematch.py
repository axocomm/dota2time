import json

def main():
    league_txt = open("leagues.json").read()
    league_json = json.loads(league_txt)
    leagues = league_assign(league_json["result"])

def league_assign(leagues):
    league_dict = {}
    for league in leagues["leagues"]:
        league_split = league["name"].split("_")
        league_dict[league["leagueid"]] = {"name": " ".join(league_split)[11:], "url": league["tournament_url"]}

    return league_dict

if __name__ == "__main__":
    main()
