#!/usr/bin/env python -O

from requests import get

class Dota:
    URL_FORMAT = 'http://api.steampowered.com/IDOTA2Match_570/%s/v1/?key=%s'

    def __init__(self, api_key):
        """Initialise a new API with the given API key."""
        self.api_key = api_key

    def get_league_listing(self):
        """Get the list of leagues."""
        return self.request('GetLeagueListing')

    def get_live_league_games(self):
        """Get the live league games."""
        return self.request('GetLiveLeagueGames')

    def get_match_details(self, match_id):
        """Get the match details."""
        return self.request('GetMatchDetails', {'match_id': match_id})

    def get_match_history(self, extra_params={}):
        """Get the match history.

        Accepts optional parameters:
          game_mode:uint32
          skill:uint32
          date_min:uint32
          date_max:uint32
          min_players:uint32
          account_id:string
          league_id:string
          start_at_match_id:string
          matches_requested:string
          tournament_games_only:string
        """
        return self.request('GetMatchHistory', extra_params)

    def get_match_history_by_sequence_num(self, extra_params={}):
        """Get match history by sequence number.

        Accepts optional parameters:
          start_at_match_seq_num:uint64
          matches_requested:uint32
        """
        return self.request('GetMatchHistoryBySequenceNumbers', extra_params)

    def get_scheduled_league_games(self, extra_params={}):
        """Get scheduled league games.

        Accepts optional parameters:
          date_min:uint32
          date_max:uint32
        """
        return self.request('GetScheduledLeagueGames', extra_params)

    def get_team_info_by_team_id(self, extra_params={}):
        """Get team information by team ID.

        Accepts optional parameters:
          start_at_team_id:uint64
          teams_requested:uint32
        """
        return self.request('GetTeamInfoByTeamID', extra_params)

    def get_tournament_player_stats(self, account_id, extra_params={}):
        """Get tournament player statistics.

        Accepts optional parameters:
          league_id:string
          hero_id:string
          time_frame:string
        """
        extra_params['account_id'] = account_id
        return self.request('GetTournamentPlayerStats', extra_params)

    def get_rarities(self, extra_params={}):
        """TODO: Get item rarities.

        Accepts language:string optional parameter
        """
        # return self.request('GetRarities', extra_params)
        return ''

    def get_heroes(self, extra_params={}):
        """TODO: Get the heroes.

        Accepts optional parameters:
          language:string
          itemizedonly:bool
        """
        # return self.request('GetHeroes', extra_params)
        return ''

    def get_tournament_prize_pool(self, extra_params={}):
        """TODO: Get the prize pool.

        Accepts leagueid:int optional parameter
        """
        # return self.request('GetTournamentPrizePool', extra_params)
        return ''

    def request(self, method, extra_params={}):
        """Make a request to url and return JSON result."""
        url = Dota.URL_FORMAT % (method, self.api_key)
        if extra_params:
            url += '&' + '&'.join(['%s=%s' % (k, extra_params[k])
                                   for k in extra_params])

        result = get(url).json()

        return result

def main():
    api = Dota('8C196C8AAA4A4D2BAAA5503A190FBA72')
    print(api.get_live_league_games())

if __name__ == '__main__':
    main()
