import json

def main():
    item_txt = open("items.json").read()
    item_json = json.loads(item_txt)
    items = item_assign(item_json["items"])

def item_assign(items):
    item_dict = {}
    for item in items:
        item_split = item["name"].split("_")
        item_split = map(ucfirst, item_split)
        item_dict[item["id"]] = " ".join(item_split)
    return item_dict

def ucfirst(item_name):
    return item_name[0].upper() + item_name[1:]

if __name__ == "__main__":
    main()
